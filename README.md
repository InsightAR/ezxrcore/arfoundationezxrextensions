# ARFoundation EZXR Extensions Samples

ARFoundation EZXR Extensions 是在Unity ARFoudation中使用易现自研算法的Samples。

## 对应版本

| Unity Version | ARFoundation EZXR Extensions Samples |
| ------------- | ------------------------------------ |
| 2022.3        | 0.1.0                                |

## 使用步骤

请事先安装要Unity2022.3，XCode及Android Studio，安装和使用请查阅相应官方文档。

### 1. 导入EZXR Unity Package Manager

https://gitlab.com/InsightAR/ezxrcore

可以使用git url的方式加载UPM。main分支为最新的分支，适合开发状态下测试。其他tag分支为release状态，适合对外提供。

1）[EZXRCommon](https://gitlab.com/InsightAR/ezxrcore/ezxrcommon)基础模块，必选

"com.ezxr.commom":"https://gitlab.com/InsightAR/ezxrcore/ezxrcommon.git"

2）[EZXRSpatialComputing](https://gitlab.com/InsightAR/ezxrcore/ezxrspatialcomputing)空间计算

"com.ezxr.spatialcomputing":"https://gitlab.com/InsightAR/ezxrcore/ezxrspatialcomputing.git"

3）[EZXRObjectDetection](https://gitlab.com/InsightAR/ezxrcore/ezxrobjectdetection)物体检测

"com.ezxr.objectdetection":"https://gitlab.com/InsightAR/ezxrcore/ezxrobjectdetection.git"

4）[EZXRTracking2d](https://gitlab.com/InsightAR/ezxrcore/ezxrtracking2d) 图片跟踪

"com.ezxr.tracking2d":"https://gitlab.com/InsightAR/ezxrcore/ezxrtracking2d.git"

5）[EZXRSemanticSegmentationHuman](https://gitlab.com/InsightAR/ezxrcore/ezxrsemanticsegmentationhuman) 人体检测

"com.ezxr.semanticsegmentationhuman":"https://gitlab.com/InsightAR/ezxrcore/ezxrsemanticsegmentationhuman.git"

6）[ARFoundationEZXRExtensions](https://gitlab.com/InsightAR/ezxrcore/arfoundationezxrextensions) Unity ARFoundation EZXR Extensions，必选

"com.ezxr.arfoundation_extension":"https://gitlab.com/InsightAR/ezxrcore/arfoundationezxrextensions.git"

Unity创建空3D工程，或者打开已有的Untiy工程，在Window -> Package Manager，选择 from disk or from url。必须先加载EZXRCommon模块，后续加载需要需要算法模块，最后再加载 ARFoundationEZXR Extensions。

Ps. 导入过程可能会碰到以下问题：

​														<img src="Documentation~/image/3.png" alt="3" style="zoom:50%;" />  

<img src="Documentation~/image/4.png" alt="4" style="zoom:50%;" />

请选择 YES、Go Ahead。

选择URL加载的方式可能会碰到下面的问题：

An error occurred while resolving packages: Project has invalid dependencies: com.ezxr.commom: Error when executing git command. fatal: could not read Username for terminal prompts disabled

可以尝试按照以下方式解决：

1. Run the following command: `git config --global credential.helper cache`

2. Run the following command: `git config --global credential.helper 'cache --timeout=3600'`

3. Run the following command: `git config --list`

   Check if the `user.name` and `user.email` fields are set correctly. If not, set them using the following commands:

   - `git config --global user.name "Your Name"`
   - `git config --global user.email "your.email@example.com"`

4. Run the following command to check if your credentials are working: `git ls-remote https://g.hz.netease.com/ezxrcore/release/ezxrcommon.git`

### 2. 设置XR Plug-in Management

Edit -> Project Settings->XR Plug-in Management，按下图依次Windows,Mac,Linux\Android\iOS的Plugin Providers。

<img src="Documentation~/image/9.png" alt="9" style="zoom:33%;" /><img src="Documentation~/image/10.png" alt="10" style="zoom:33%;" /><img src="Documentation~/image/11.png" alt="11" style="zoom:33%;" />

 

### 3. 导入Samples

举例：导入EZXRSemanticSegmentationHuman Foundation Sample

1）在Package Manager选择ARFoundationEZXRExtensions，在Samples中，选择import。

<img src="Documentation~/image/6.png" alt="6" style="zoom:50%;" />

2）导入EZXRSemanticSegmentationHuman需要的算法模型文件

在Package Manager选择EZXRSemanticSegmentationHuman ，在Samples中，选择import。并且把StreamingAssets文件夹整体剪贴到Unity Assets目录下。

<img src="Documentation~/image/7.png" alt="7" style="zoom:50%;" />

完成前面4步骤后，Unity工程文件组织结构如下，请检测是否一致：

<img src="Documentation~/image/8.png" alt="8" style="zoom:70%;" />



3）打开SemanticSegmentationHuman Scene

因为SemanticSegmentationHuman Scene的UI有用到TextMesh Pro，所以需要导入下TextMesh Pro的资源。

<img src="Documentation~/image/12.png" alt="12" style="zoom:50%;" />



### 4. 构建打包IOS应用

File->Build Settings->iOS->Switch Platform，并且Add Open Scenes 添加需要打包的Scene。针对当前例子，可以只添加SemanticSegmentationHuman Scene，这样后面应用进去就是ar界面。为了模拟AR应用进入和退出，可以先 add Menu scense，再添加SemanticSegmentationHuman Scene。这样应用会先打开Menu菜单界面，再选择进入SemanticSegmentationHuman Scene。（注意这里Menu Scene的序列 一定要是0）

<img src="Documentation~/image/13.png" alt="13" style="zoom:50%;" />

#### 4-1 iOS Unity设置

打开File->Build Settings->Player Settings

1）Player - Company Name、Product Name、Version 根据项目自定义

2）Resolution and Presentation - Oriebtation

3）Other Settings - Identification 和 iOS Provisioning Profile 根据项目自定义，可以不用在Unity配置，也可以在导出的XCode的工程里面配置

4）Other Settings - Configuration - Camera Usage Description* 需要写请求相机的原因

5）Other Settings - Configuration - Allow downloads over HTTP* 如果使用到http的api 必须选择  Always allowed

6）Other Settings - Script Compilation - Allow ‘unsafe’ Code 勾选中



<img src="Documentation~/image/14.png" alt="14" style="zoom:50%;" />

<img src="Documentation~/image/15.png" alt="15" style="zoom:50%;" />

#### 4-2 iOS XCode设置

Build Settings界面的build的界面，点击Build按钮，选择导出目录导出。（后续项目有什么修改，需要再次到处请选择Append的方式，这样就可以保留在XCode项目做的修改，否则会被覆盖）

1）添加OpenCV库

目前ezxr用的opencv版本是3.4.1（注意其他版本可能有api不兼容的问题，碰到请联系ezxr对接人）。可与从官网下载opencv 3.4.1的framework，也可以使用ARFoundationEZXRExtensions -> ThirdParty~ 文件夹下面的opencv2.framework。将opencv2.framework copy到刚才到处的XCode工程的根目录下，打开Xcode工程，选择Targets中的Unity Framework -> General -> Frameworks and Libraries 将opencv2.framework 添加进来。

<img src="Documentation~/image/17.png" alt="17" style="zoom:50%;" />

2）关闭bitcode

选择Targets中的Unity Framework和Unity-iPhone，在Build Settings, 搜索bitcode，选择NO，如图：

<img src="Documentation~/image/18.png" alt="18" style="zoom:50%;" />

3）配置Signing & Capabilities

根据iOS开发需要自行配置，如有问题，请参阅相应官网。

#### 4-3 运行应用

build 没有错误后，就可以连接真机运行，查看具体AR效果，确保iOS真机设备带有LiDAR。

### 5. 构建打包Android应用

File->Build Settings->Android->Switch Platform，并且Add Open Scenes 添加需要打包的Scene。针对当前例子，可以只添加SemanticSegmentationHuman Scene，这样后面应用进去就是ar界面。为了模拟AR应用进入和退出，可以先 add Menu scense，再添加SemanticSegmentationHuman Scene。这样应用会先打开Menu菜单界面，再选择进入SemanticSegmentationHuman Scene。（注意这里Menu Scene的序列 一定要是0）

Android勾选Export Project，方便在Android Studio修改导出的工程

<img src="Documentation~/image/21.png" alt="21" style="zoom:50%;" />

#### 5-1 Android Unity设置

打开File->Build Settings->Player Settings

1）Player - Company Name、Product Name、Version 根据项目自定义

2）Resolution and Presentation - Oriebtation

6）Other Settings - Script Compilation - Allow ‘unsafe’ Code 勾选中

7）Other Settings - Rendering 取消勾选 Auto Graphics API 并移除 Vulkan

3）Other Settings - Minimum API Level 需要 选择24及以上

4）Other Settings - Configuration - Camera Usage Description* 需要写请求相机的原因

5）Other Settings - Configuration - Allow downloads over HTTP*  用到http的api 必须选择  Always allowed

8）Other Settings - Configuration - Scripting Backend 选择IL2CPP

9）Other Settings - Configuration - Target Architectures 勾选上ARM64

<img src="Documentation~/image/25.png" alt="25" style="zoom:50%;" />

<img src="Documentation~/image/26.png" alt="26" style="zoom:50%;" />

<img src="Documentation~/image/31.png" alt="31" style="zoom:50%;" />

#### 5-2 Android Studio 构建运行应用

Gradle build 没有错误后，就可以连接真机运行，查看具体AR效果。
