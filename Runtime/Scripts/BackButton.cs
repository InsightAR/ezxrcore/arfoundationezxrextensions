using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour
{
    private GameObject m_BackButton;
    // Start is called before the first frame update
    void Start()
    {
        m_BackButton = this.gameObject;
        if (Application.CanStreamedLevelBeLoaded("Menu"))
                m_BackButton.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackButtonPressed()
    {
        if (Application.CanStreamedLevelBeLoaded("Menu"))
            SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }
}
