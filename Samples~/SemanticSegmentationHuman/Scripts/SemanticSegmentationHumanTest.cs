using System.Collections.Generic;
using EzxrCore.Common;
using EZXRCoreExtensions.SemanticSegmentationHuman;
using UnityEngine;

public enum IdentificationState : int
{
    None = 0,
    Detecting = 1,
    Sucessed = 2,
    Failed = 3,
}

public class HumanGo
{
    public GameObject go;
    public HumanInfo info;

    public IdentificationState state;
    public string Identification;
    public HumanGo(GameObject go, HumanInfo info)
    {
        this.go = go;
        this.info = info;
        this.state = IdentificationState.None;
        this.Identification = "";
    }
}

public class SemanticSegmentationHumanTest : MonoBehaviour
{
    public ARHumanDetTrackInfo m_humanDetTrackInfo;
    public GameObject m_humanBoundsPrefab;
    

    private List<HumanGo> m_humanBoundsGo = new List<HumanGo>();
    private ImageIdentificationTest m_imageIdentification;
    // Start is called before the first frame update
    void Start()
    {
        if (m_humanDetTrackInfo == null)
        {
            Debug.LogException(new System.NullReferenceException(
                $"Serialized properties were not initialized on {name}'s {nameof(SemanticSegmentationHumanTest)} component."), this);
            return;
        }

        if (m_imageIdentification == null)
        {
           m_imageIdentification = GetComponent<ImageIdentificationTest>();
        }
        
        m_humanBoundsGo.Clear();
        m_humanDetTrackInfo.m_OnAddHumanInfo += OnAddHumanInfo;
        m_humanDetTrackInfo.m_OnRemoveHumanInfo += OnRemoveHumanInfo;
        m_humanDetTrackInfo.m_OnUpdateHumanInfo += OnUpdateHumanInfo;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGUI()
    {
        for (int i = 0; i < m_humanBoundsGo.Count; i++)
        {
            HumanInfo humanDetTrackInfo2d = m_humanBoundsGo[i].info;
            GUIHelper.DrawRect(new Rect(humanDetTrackInfo2d.x, humanDetTrackInfo2d.y, humanDetTrackInfo2d.width, humanDetTrackInfo2d.height));
            string label = string.Format("human label {0} state {1} id {2}", humanDetTrackInfo2d.label, m_humanBoundsGo[i].state, m_humanBoundsGo[i].Identification);
            GUI.skin.label.fontSize = 50;
            GUI.skin.label.normal.textColor = Color.red;
            GUI.Label(new Rect(humanDetTrackInfo2d.x, humanDetTrackInfo2d.y, humanDetTrackInfo2d.width, humanDetTrackInfo2d.height), label);
        }
    }

    void OnDestroy()
    {
        if (m_humanDetTrackInfo != null)
        {
            m_humanDetTrackInfo.m_OnAddHumanInfo -= OnAddHumanInfo;
            m_humanDetTrackInfo.m_OnRemoveHumanInfo -= OnRemoveHumanInfo;
            m_humanDetTrackInfo.m_OnUpdateHumanInfo -= OnUpdateHumanInfo;
        }
   
    }

    private void OnAddHumanInfo(List<HumanInfo> humanInfos)
    {
        string strlog = "";
        foreach (var humanInfo in humanInfos)
        {
            strlog = strlog + " " + humanInfo.label;
            var go = Instantiate(m_humanBoundsPrefab);
            go.transform.position = humanInfo.bounds.center;
            go.transform.localScale = humanInfo.bounds.size;
            var humanGo = new HumanGo(go, humanInfo);
            m_humanBoundsGo.Add(humanGo);
            m_imageIdentification?.GetIdentification(humanGo);
        }
        Debug.Log("OnAddHumanInfo " + strlog);
    }

    private void OnRemoveHumanInfo(List<HumanInfo> humanInfos)
    {
        string strlog = "";
        foreach (var humanInfo in humanInfos)
        {
            strlog = strlog + " " + humanInfo.label;
            var go = m_humanBoundsGo.Find(x => x.info.label == humanInfo.label);
            if (go.go != null)
            {
                m_humanBoundsGo.Remove(go);
                Destroy(go.go);
                Resources.UnloadUnusedAssets();
            }
        }
        Debug.Log("OnRemoveHumanInfo " + strlog);
    }

    private void OnUpdateHumanInfo(List<HumanInfo> humanInfos)
    {
        string strlog = "";
        foreach (var humanInfo in humanInfos)
        {
            strlog = strlog + " " + humanInfo.label;
            for (int i = 0; i < m_humanBoundsGo.Count; i++)
            {
                if (m_humanBoundsGo[i].info.label == humanInfo.label)
                {
                    var go = m_humanBoundsGo[i];
                    go.go.transform.position = humanInfo.bounds.center;
                    go.go.transform.localScale = humanInfo.bounds.size;
                    go.info = humanInfo;
                    m_humanBoundsGo[i] = go;
                    m_imageIdentification?.GetIdentification(go);
                    break;
                }
            }
        }
        Debug.Log("OnUpdateHumanInfo " + strlog);
    }

   
}
