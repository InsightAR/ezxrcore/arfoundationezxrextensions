using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
namespace EZXRCoreExtensions.SemanticSegmentationHuman
{
    public class HumanInfo
    {
        public float x;
        public float y;
        public float width;
        public float height;
        public int label;
        public Bounds bounds;
        public Queue<Bounds> boundsList = new Queue<Bounds>();
    }

    public class HumanInfoComparer : IEqualityComparer<HumanInfo>
    {
        public bool Equals(HumanInfo x, HumanInfo y)
        {
            return x.label == y.label;
        }

        public int GetHashCode(HumanInfo obj)
        {
            return obj.label.GetHashCode();
        }
    }

    public class ARHumanDetTrackInfo : MonoBehaviour
    {
        public ARCameraManager m_cameraManager;
        public ARRaycastManager m_RaycastManager;
        public AROcclusionManager m_OcclusionManager;

        [NonSerialized]
        public Action<List<HumanInfo>> m_OnAddHumanInfo = null;
        [NonSerialized]
        public Action<List<HumanInfo>> m_OnRemoveHumanInfo = null;
        [NonSerialized]
        public Action<List<HumanInfo>> m_OnUpdateHumanInfo = null;

        private List<HumanInfo> m_humanInfos = new List<HumanInfo>();

        private Transform m_cameraTransform;
        private Camera m_camera;
        private IntPtr m_depthImage = IntPtr.Zero;
        private int m_depthImageWidth = 0;
        private int m_depthImageHeight = 0;

        private float m_depthMax = 6.0f;
        private float m_depthMin = 0.3f;
        private float m_humanHeightMax = 2.0f;
        private float m_humanHeightMin = 0.5f;
        private float m_humanWidth = 0.4f;
        // Start is called before the first frame update
        void Start()
        {
            if (m_cameraManager == null)
            {
                Debug.LogException(new NullReferenceException(
                    $"Serialized properties were not initialized on {name}'s {nameof(ARCameraManager)} component."), this);
                return;
            }
            if (m_OcclusionManager == null)
            {
                Debug.LogException(new NullReferenceException(
                    $"Serialized properties were not initialized on {name}'s {nameof(ARRaycastManager)} component."), this);
                return;
            }
            m_cameraTransform = m_cameraManager.gameObject.transform;
            m_camera = m_cameraManager.GetComponent<Camera>();
            m_humanInfos.Clear();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void UpdateHumanDetTrackInfo(List<HumanDetTrackInfo> humanDetTrackInfos, int imageWidth, int imageHeight)
        {
            if (humanDetTrackInfos == null)
            {
                return;
            }

            List<HumanInfo> humanInfos_new = new List<HumanInfo>();
            if (m_OcclusionManager.TryAcquireHumanDepthCpuImage(out XRCpuImage image))
            {
                unsafe
                {
                    m_depthImageWidth = image.width;
                    m_depthImageHeight = image.height;
                    XRCpuImage.Plane plane = image.GetPlane(0);
                    m_depthImage = new IntPtr(plane.data.GetUnsafePtr());
                    //Debug.Log("image = " + image.width + " " + image.height + " " + image.format + " " +  plane.rowStride + " " + plane.pixelStride);
                }
            }

            for (int i = 0; i < humanDetTrackInfos.Count; i++)
            {
                HumanInfo humanInfo = GetHumanInfoFromHumanDepth(humanDetTrackInfos[i], imageWidth, imageHeight);
                humanInfos_new.Add(humanInfo);
            }
            
            if (image != null)
            {
                image.Dispose();
                m_depthImage = IntPtr.Zero;
            }
            
            
            // Find the common elements between the two lists
            List<HumanInfo> commonElements = new List<HumanInfo>();
            foreach (HumanInfo element_new in humanInfos_new)
            {
                foreach (HumanInfo element_old in m_humanInfos)
                {
                    if (element_new.label == element_old.label)
                    {
                        commonElements.Add(element_new);
                        break;
                    }
                }
            }
            UpdateHumanInfo(commonElements);
            
            // Find the elements in m_humanInfos that are not in humanInfos_new
            List<HumanInfo> reomveElements = m_humanInfos.Except(humanInfos_new, new HumanInfoComparer()).ToList();
            RemoveHumanInfo(reomveElements);

            // Find the elements in humanInfos_new that are not in m_humanInfos
            List<HumanInfo> addElements = humanInfos_new.Except(m_humanInfos, new HumanInfoComparer()).ToList();
            AddHumanInfo(addElements);
        }

        private void AddHumanInfo(List<HumanInfo> humanInfos)
        {
            if (humanInfos != null && humanInfos.Count > 0 && m_OnAddHumanInfo != null)
            {
                for(int i = 0; i < humanInfos.Count; i++)
                {
                    HumanInfo humanInfo = humanInfos[i];
                    humanInfo.boundsList = new Queue<Bounds>();
                    humanInfo.boundsList.Enqueue(humanInfo.bounds);
                    m_humanInfos.Add(humanInfo);
                }
                m_OnAddHumanInfo(humanInfos);
            }
        }

        private void RemoveHumanInfo(List<HumanInfo> humanInfos)
        {
            if (humanInfos != null && humanInfos.Count > 0 && m_OnRemoveHumanInfo != null)
            {
                for (int i = 0; i < humanInfos.Count; i++)
                {
                    HumanInfo humanInfo = humanInfos[i];
                    m_humanInfos.Remove(humanInfo);
                }
                m_OnRemoveHumanInfo(humanInfos);
            }
        }

        private void UpdateHumanInfo(List<HumanInfo> humanInfos)
        {
            if (humanInfos != null && humanInfos.Count > 0 && m_OnUpdateHumanInfo != null)
            {
                for (int i = 0; i < humanInfos.Count; i++)
                {
                    HumanInfo humanInfo = humanInfos[i];
                    for(int j = 0; j < m_humanInfos.Count; j++)
                    {
                        if(humanInfo.label == m_humanInfos[j].label)
                        {
                            if(m_humanInfos[j].boundsList.Count >= 5)
                            {
                                m_humanInfos[j].boundsList.Dequeue();
                            }
                            m_humanInfos[j].boundsList.Enqueue(humanInfo.bounds);
                            Bounds bounds_new = new Bounds(Vector3.zero, Vector3.zero);
                            for(int k = 0; k < m_humanInfos[j].boundsList.Count; k++)
                            {
                                bounds_new.center = bounds_new.center + m_humanInfos[j].boundsList.ElementAt(k).center;
                                bounds_new.size = bounds_new.size + m_humanInfos[j].boundsList.ElementAt(k).size;
                            }
                            bounds_new.center = bounds_new.center / m_humanInfos[j].boundsList.Count;
                            bounds_new.size = bounds_new.size / m_humanInfos[j].boundsList.Count;
                            m_humanInfos[j].bounds = bounds_new;
                            m_humanInfos[j].x = humanInfo.x;
                            m_humanInfos[j].y = humanInfo.y;
                            m_humanInfos[j].width = humanInfo.width;
                            m_humanInfos[j].height = humanInfo.height;
                            humanInfo.bounds = bounds_new;
                            break;
                        }
                    }
                }
                m_OnUpdateHumanInfo(humanInfos);
            }
        }

        private HumanInfo GetHumanInfoFromHumanDepth(HumanDetTrackInfo humanDetTrackInfo, int imageWidth, int imageHeight)
        {
            if(Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
            {
                //目前返回时竖屏图像
                int temp = imageWidth;
                imageWidth = imageHeight;
                imageHeight = temp;
            }
            
            float scale_screen = (float)Screen.width / Screen.height;
            float scale_image = (float)imageWidth / imageHeight;
            float human_x, human_y, human_width, human_height;
            if (scale_screen > scale_image)
            {
                float scale = (float)Screen.width / imageWidth;
                float offset_y = (imageHeight * scale - Screen.height) / 2;
                human_x = humanDetTrackInfo.x1 * scale;
                human_y = humanDetTrackInfo.y1 * scale - offset_y;
                human_width = humanDetTrackInfo.width * scale;
                human_height = humanDetTrackInfo.height * scale;
            }
            else
            {
                float scale = (float)Screen.height / imageHeight;
                float offset_x = (imageWidth * scale - Screen.width) / 2;
                human_x = humanDetTrackInfo.x1 * scale - offset_x;
                human_y = humanDetTrackInfo.y1 * scale;
                human_width = humanDetTrackInfo.width * scale;
                human_height = humanDetTrackInfo.height * scale;
            }
            float depthValue = 0.0f;
            if(m_depthImage != IntPtr.Zero)
            {   
                
                if(Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
                {
                    int imgW = imageHeight;
                    int imgH = imageWidth;
                    float human_center_x = (humanDetTrackInfo.y1 + humanDetTrackInfo.height / 2) * m_depthImageWidth / imgW;
                    float human_center_y = (imgH - (humanDetTrackInfo.x1 + humanDetTrackInfo.width / 2)) * m_depthImageHeight / imgH;
                    float pixelIndex = human_center_y * m_depthImageWidth + human_center_x;
                    int depthValueIndex = Mathf.Clamp((int)pixelIndex, 0, m_depthImageWidth * m_depthImageHeight - 1);
                    depthValue = Marshal.PtrToStructure<float>(m_depthImage + depthValueIndex * sizeof(float));
                    Debug.Log("human_center_x = " + human_center_x + " human_center_y = " + human_center_y + " " + "color = " + depthValue + " " + m_depthImageWidth + " " + m_depthImageHeight + " " + imgW + " " + imgH);
                }
                else
                {
                    int imgW = imageWidth;
                    int imgH = imageHeight;
                    float human_center_x = (humanDetTrackInfo.x1 + humanDetTrackInfo.width /  2) * m_depthImageWidth  / imgW;
                    float human_center_y = (humanDetTrackInfo.y1 + humanDetTrackInfo.height / 2) * m_depthImageHeight / imgH;
                    float pixelIndex = human_center_y * m_depthImageWidth + human_center_x;
                    int depthValueIndex = Mathf.Clamp((int)pixelIndex, 0, m_depthImageWidth * m_depthImageHeight - 1);
                    depthValue = Marshal.PtrToStructure<float>(m_depthImage + depthValueIndex * sizeof(float));
                    Debug.Log("human_center_x = " + human_center_x + " human_center_y = " + human_center_y + " " + "color = " + depthValue + " " + m_depthImageWidth + " " + m_depthImageHeight + " " + imgW + " " + imgH);
                }
            }
            else
                Debug.Log("envDepth2 = null");

            Bounds humanBounds = new Bounds(Vector3.zero, Vector3.zero);
            if (m_camera != null)
            {
                if (depthValue > 0.01f)
                {
                    depthValue = Mathf.Clamp(depthValue, m_depthMin, m_depthMax);
                    Vector2 sceenPoint_centre = new Vector2(human_x + human_width / 2, Screen.height - (human_y + human_height / 2));
                    Vector3 position3d_centre = m_camera.ScreenToWorldPoint(new Vector3(sceenPoint_centre.x, sceenPoint_centre.y, depthValue));
                    Vector2 sceenPoint_top = new Vector2(human_x + human_width / 2, Screen.height - human_y);
                    Vector3 position3d_top = m_camera.ScreenToWorldPoint(new Vector3(sceenPoint_top.x, sceenPoint_top.y, depthValue));
                    float height = 2.0f*Mathf.Abs(position3d_top.y - position3d_centre.y);
                    height = Mathf.Clamp(height, m_humanHeightMin, m_humanHeightMax);
                    Debug.Log("GetHumanInfoFromHumanDepth height = " + height + " depth = " + depthValue + " " + position3d_centre + " " + position3d_top);
                    Vector3 new_centre = position3d_centre;
                    humanBounds = new Bounds(new_centre, new Vector3(m_humanWidth, height, m_humanWidth));
                }
                else
                {
                    Vector2 sceenPoint_centre = new Vector2(human_x + human_width / 2, Screen.height - (human_y + human_height / 2));
                    Vector3 position3d_centre = Get3dPointFromSceenPoint(sceenPoint_centre);
                    Vector3 direction = new Vector3(position3d_centre.x, 0, position3d_centre.z) - new Vector3(m_cameraTransform.position.x, 0, m_cameraTransform.position.z);
                    float depth = direction.magnitude;
                    depth = Mathf.Clamp(depth, m_depthMin, m_depthMax);
                    if (position3d_centre != Vector3.zero && m_camera != null)
                    {
                        Vector2 sceenPoint_top = new Vector2(human_x + human_width / 2, Screen.height - human_y);
                        Vector3 position3d_top = m_camera.ScreenToWorldPoint(new Vector3(sceenPoint_top.x, sceenPoint_top.y, depth));
                        float height = 2.0f*Mathf.Abs(position3d_top.y - position3d_centre.y);
                        height = Mathf.Clamp(height, m_humanHeightMin, m_humanHeightMax);
                        Debug.Log("GetHumanInfoFromHumanDepth2 height = " + height + " depth = " + depth + " " + position3d_centre + " " + position3d_top);
                        Vector3 new_centre = position3d_centre;
                        humanBounds = new Bounds(new_centre, new Vector3(m_humanWidth, height, m_humanWidth));
                    }
                }
            }
            
            HumanInfo humanInfo = new HumanInfo();
            humanInfo.x = human_x;
            humanInfo.y = human_y;
            humanInfo.width = human_width;
            humanInfo.height = human_height;
            humanInfo.label = humanDetTrackInfo.curr_id;
            humanInfo.bounds = humanBounds;
            return humanInfo;
        }

        private Vector3 Get3dPointFromSceenPoint(Vector2 position)
        {
            const TrackableType trackableTypes = TrackableType.FeaturePoint |  TrackableType.Depth;
            List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
            // Perform the raycast
            if (m_RaycastManager.Raycast(position, s_Hits, trackableTypes))
            {
                for(int i = 0; i < s_Hits.Count; i++)
                {
                    Debug.Log("s_Hits[" + i + "] = " + s_Hits[i].pose.position + " " + s_Hits[i].hitType + " " + s_Hits[i].distance);
                }
                // Raycast hits are sorted by distance, so the first one will be the closest hit.
                var hit = s_Hits[s_Hits.Count-1];
                return hit.pose.position;
            }
            return Vector3.zero;
        }

        private string GetStr(List<HumanInfo> humanInfos)
        {
            string str = "";
            foreach (HumanInfo humanInfo in humanInfos)
            {
                str += humanInfo.label + " " + humanInfo.x + " " + humanInfo.y + " " + humanInfo.width + " " + humanInfo.height + "\n";
            }
            return str;
        }
    }
}