using System.Collections.Generic;
using EzxrCore.Network.Http;
using EZXRCoreExtensions.SemanticSegmentationHuman;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.XR.ARFoundation;

[System.Serializable]
public class PostBody
{
    public string img_base64;
    public PostBody(string img_base64)
    {
        this.img_base64 = img_base64;
    }
}

public class ImageIdentificationTest : MonoBehaviour
{
    public ARCameraBackground m_ARCameraBackground;

    private RenderTexture m_RenderTexture;
    private Texture2D m_roiTexure;

    private string url = "http://39.98.230.88:7893/api/v1/query_image";
    private Dictionary<int, string> identificationDictionary = new Dictionary<int, string>();

    private int identificationFailedMaxCount = 10;
    private Dictionary<int, int> identificationFailedCountDictionary = new Dictionary<int, int>();

    private List<HumanGo> m_needIdentificationHumanGoList = new List<HumanGo>();
    // Start is called before the first frame update
    

    void Start()
    {
        identificationDictionary.Clear();
        identificationFailedCountDictionary.Clear();
        m_RenderTexture = new RenderTexture(Screen.width, Screen.height, 24);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_needIdentificationHumanGoList.Count > 0)
        {
            // Create a new command buffer
            var commandBuffer = new CommandBuffer();
            commandBuffer.name = "AR Camera Background Blit Pass";

            // Get a reference to the AR Camera Background's main texture
            // We will copy this texture into our chosen render texture
            var texture = !m_ARCameraBackground.material.HasProperty("_MainTex") ?
                null : m_ARCameraBackground.material.GetTexture("_MainTex");

            // Save references to the active render target before we overwrite it
            var colorBuffer = Graphics.activeColorBuffer;
            var depthBuffer = Graphics.activeDepthBuffer;

            // Set Unity's render target to our render texture
            Graphics.SetRenderTarget(m_RenderTexture);

            // Clear the render target before we render new pixels into it
            commandBuffer.ClearRenderTarget(true, false, Color.clear);

            // Blit the AR Camera Background into the render target
            commandBuffer.Blit(
                texture,
                BuiltinRenderTextureType.CurrentActive,
                m_ARCameraBackground.material);

            // Execute the command buffer
            Graphics.ExecuteCommandBuffer(commandBuffer);

            // Set Unity's render target back to its previous value
            Graphics.SetRenderTarget(colorBuffer, depthBuffer);
            Debug.Log("m_needIdentificationHumanGoList.Count:" + m_needIdentificationHumanGoList.Count);
            for (int i = 0; i < m_needIdentificationHumanGoList.Count; i++)
            {
                SendRequest(m_needIdentificationHumanGoList[i]);
            }
            m_needIdentificationHumanGoList.Clear();
        }
    }

    void OnDestroy()
    {
        if (m_RenderTexture != null)
        {
            m_RenderTexture.Release();
            m_RenderTexture = null;
        }
    }

    // void OnGUI()
    // {
    //     if (m_RenderTexture != null && m_roiTexure != null)
    //     {
    //         GUI.DrawTexture(new Rect(0, 0, m_roiTexure.width, m_roiTexure.height), m_roiTexure, ScaleMode.ScaleToFit);
    //     }
    // }

    public void GetIdentification(HumanGo humanGo)
    {
        if (humanGo.state == IdentificationState.None || humanGo.state == IdentificationState.Failed)
        {
            if (identificationDictionary.ContainsKey(humanGo.info.label))
            {
                humanGo.Identification = identificationDictionary[humanGo.info.label];
                humanGo.state = IdentificationState.Sucessed;
                return;
            }

            if (identificationDictionary.ContainsKey(humanGo.info.label))
            {
                if (identificationFailedCountDictionary[humanGo.info.label] >= identificationFailedMaxCount)
                {
                    humanGo.state = IdentificationState.Failed;
                    humanGo.Identification = "Identification field max count";
                    return;
                }
            }

            humanGo.state = IdentificationState.Detecting;
            m_needIdentificationHumanGoList.Add(humanGo);
        }
    }

    private void SendRequest(HumanGo humanGo)
    {
        HttpRequest request = new HttpRequest(HttpMethod.POST);
        request.url = url;
        //获取图片base64
        PostBody body = new PostBody(GetImagebase64(humanGo.info));
        request.requestBody = RequestBody.From<PostBody>(body);
        HttpClient.SendRequest(request, (response) => {
            if(!response.IsOK())
            {
                Debug.LogError("SendRequest failed: " + response.error);
                humanGo.state = IdentificationState.Failed;
                if (identificationFailedCountDictionary.ContainsKey(humanGo.info.label))
                {
                    identificationFailedCountDictionary[humanGo.info.label] += 1;
                }
                else
                {
                    identificationFailedCountDictionary.Add(humanGo.info.label, 1);
                }
                return;
            }
            try
            {
                Debug.Log("SendRequest success: " + response.body);
                humanGo.Identification = response.body;
                identificationDictionary.Add(humanGo.info.label, humanGo.Identification); 
                humanGo.state = IdentificationState.Sucessed;
                return;
            }
            catch(System.Exception ex)
            {
                Debug.LogError("SendRequest exception: " + ex.Message);
                humanGo.state = IdentificationState.Failed;
                return;
            }
        });
    }

    private string GetImagebase64(HumanInfo humanInfo)
    {
        humanInfo.x = Mathf.Clamp(humanInfo.x, 0, Screen.width-1);
        humanInfo.y = Mathf.Clamp(humanInfo.y, 0, Screen.height-1);
        humanInfo.width = Mathf.Clamp(humanInfo.width, 1, Screen.width);
        humanInfo.height = Mathf.Clamp(humanInfo.height, 1, Screen.height);
        Rect captureRect = new Rect(humanInfo.x, humanInfo.y, humanInfo.width, humanInfo.height);
        if (m_roiTexure == null || m_roiTexure.width != (int)captureRect.width || m_roiTexure.height != (int)captureRect.height)
        {
            m_roiTexure = new Texture2D((int)captureRect.width, (int)captureRect.height, TextureFormat.RGB24, false);
        }
        RenderTexture.active = m_RenderTexture;
        m_roiTexure.ReadPixels(captureRect, 0, 0);
        m_roiTexure.Apply();
        RenderTexture.active = null;
        byte[] bytes = m_roiTexure.EncodeToPNG();
        string base64 = System.Convert.ToBase64String(bytes);
        return base64;
    }
}
