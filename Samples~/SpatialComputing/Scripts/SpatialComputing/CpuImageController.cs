using System;
using System.Runtime.InteropServices;
using EzxrCore.Common;
using EZXRCoreExtensions.SpatialComputing;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
namespace EZXRCoreExtensions.SpatialComputing
{
    public class CpuImageController : CpuImageControllerBase
    {
        public ARCameraManager m_cameraManager;
        public ARSession m_session;
        public float m_vpsRequestFrameFrequency = 0.5f;
        public bool m_genCameraTexture = false;
        public Texture2D m_CameraTexture;
        private float m_lastRunTracking2dTime;

        private Transform m_cameraTransform;
        private bool m_fisrtFrameArrived = true;
        private double m_timestampDelta = 0;
        private double m_timestampNow = 0;

        
        private IntPtr m_imagePtr = IntPtr.Zero;
        private int m_imageLength = 0;
        void Start()
        {
            m_cameraTransform = m_cameraManager.gameObject.transform;
            m_lastRunTracking2dTime = Time.time;
        }

        void OnEnable()
        {
            if (m_cameraManager == null)
            {
                Debug.LogException(new NullReferenceException(
                    $"Serialized properties were not initialized on {name}'s {nameof(CpuImageController)} component."), this);
                return;
            }

            m_cameraManager.frameReceived += OnCameraFrameReceived;
        }

        void OnDisable()
        {
            if (m_cameraManager != null)
                m_cameraManager.frameReceived -= OnCameraFrameReceived;
        }

        public override TrackState GetTrackingState()
        {
            TrackingState trackingState = TrackingState.None;
            if (m_session.subsystem == null)
                trackingState = TrackingState.None;
            trackingState = m_session.subsystem.trackingState;
            switch (trackingState)
            {
                case TrackingState.None:
                    return TrackState.detecting;
                case TrackingState.Limited:
                    return TrackState.track_limited;
                case TrackingState.Tracking:
                    return TrackState.tracking;
                default:
                    return TrackState.detecting;
            }
        }

        void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
        {
            ProcessCameraImage(eventArgs);
        }

        unsafe void ProcessCameraImage(ARCameraFrameEventArgs eventArgs)
        {

            // Attempt to get the latest camera image. If this method succeeds,
            // it acquires a native resource that must be disposed (see below).
            if (!m_cameraManager.TryAcquireLatestCpuImage(out XRCpuImage image))
            {
                return;
            }

            if (true == m_fisrtFrameArrived)
            {
                //获取unix时间戳，秒为单位
                double unixTimestamp = UnixTimestamp.Now();
                m_timestampDelta = unixTimestamp - image.timestamp;
                m_fisrtFrameArrived = false;
            }
            m_timestampNow = m_timestampDelta + image.timestamp;

            XRCameraIntrinsics intrinsics;
            if (!m_cameraManager.TryGetIntrinsics(out intrinsics))
            {
                image.Dispose();
                return;
            }
            
            TrackState trackState = GetTrackingState();
             Matrix4x4 LocalMatrix = Matrix4x4.TRS(m_cameraTransform.localPosition, m_cameraTransform.localRotation, Vector3.one);
            OnTrackFrameProcess(LocalMatrix, trackState, m_timestampNow, Screen.orientation);

        
            float currentTime = Time.time;
            // Calculate the time interval since the last update
            float deltaTime = currentTime - m_lastRunTracking2dTime;
            if (deltaTime < m_vpsRequestFrameFrequency)
            {
                image.Dispose();
                return;
            }
            m_lastRunTracking2dTime = currentTime;

            SC_InputImage inputImage = GetSC_InputImage(intrinsics, image);
            OnImageFrameProcess(LocalMatrix, inputImage, trackState, m_timestampNow, Screen.orientation);
            // We must dispose of the XRCameraImage after we're finished
            // with it to avoid leaking native resources.
            image.Dispose();
        }

        

        SC_InputImage GetSC_InputImage(XRCameraIntrinsics intrinsics, XRCpuImage image)
        {
            SC_InputImage input_image = new SC_InputImage();
            switch (image.format)
            {
                case XRCpuImage.Format.AndroidYuv420_888:
                    input_image.format = SC_ImageFormat.SC_Image_RGB_888; //Y0,Y1...Yn,U0,U1...Un/4,V0,V1...Vn/4
                    break;
                case XRCpuImage.Format.IosYpCbCr420_8BiPlanarFullRange: //Y0,Y1...Yn,U0,V0,U1,V1...Un/2,Vn/2 NV21格式
                    input_image.format = SC_ImageFormat.SC_Image_YUV420_NV12;
                    break;
                case XRCpuImage.Format.OneComponent8:
                    input_image.format = SC_ImageFormat.SC_Image_GRAY;
                    break;
                default:
                    input_image.format = SC_ImageFormat.SC_Image_INVALID;
                    break;
            }
            unsafe
            {
                XRCpuImage.Plane plane = image.GetPlane(0);
                int dataLength = plane.data.Length;
                input_image.ptr0 = new IntPtr(plane.data.GetUnsafePtr());
                input_image.len_ptr0 = dataLength;

                if (image.planeCount == 2)
                {
                    if(m_genCameraTexture == true)
                    {
                        GetCameraTexture(image);
                    }

                    XRCpuImage.Plane plane1 = image.GetPlane(1);
                    int dataLength1 = dataLength / 2;
                    input_image.ptr1 = new IntPtr(plane1.data.GetUnsafePtr());
                    input_image.len_ptr1 = dataLength1;
                    //Debug.Log("GetSC_InputFrame plane 1: " + dataLength1.ToString() + " " + plane1.rowStride.ToString() + " " + plane1.pixelStride.ToString());
                }
                else if (image.planeCount == 3)
                {
                    GetCameraTexture(image);
                    input_image.ptr0 = m_imagePtr;
                    input_image.len_ptr0 = m_imageLength;
                    // Debug.Log("GetSC_InputFrame rgb plane : " + rawTextureData.Length.ToString());
                    // byte[] byteArray = new byte[rawTextureData.Length];
                    // Marshal.Copy(input_image.ptr0, byteArray, 0, rawTextureData.Length);
                    // EzxrCore.Log.ECLog.SaveFile(byteArray, "testRGB.bin");
                }
            }

            input_image.height = image.height;
            input_image.width = image.width;
            input_image.fx = intrinsics.focalLength.x; // 手写参数，只是为了验证数据IO的正确性
            input_image.fy = intrinsics.focalLength.y;
            input_image.cx = intrinsics.principalPoint.x; // 居中
            input_image.cy = intrinsics.principalPoint.y;
            input_image.timestamp_s = m_timestampNow;
            input_image.k1 = 0.0f;
            input_image.k2 = 0.0f;
            input_image.k3_p1 = 0.0f;
            input_image.k4_p2 = 0.0f;
            input_image.camera_type = SC_CameraType.SC_PINHOLE;

            return input_image;
        }

        private void GetCameraTexture(XRCpuImage image)
        {
            if (m_CameraTexture == null || m_CameraTexture.width != image.width || m_CameraTexture.height != image.height)
            {
                const TextureFormat format = TextureFormat.RGB24;
                m_CameraTexture = new Texture2D(image.width, image.height, format, false);
            }
            XRCpuImage.Transformation transformation = XRCpuImage.Transformation.None;
            // Convert the image to format, flipping the image across the Y axis.
            // We can also get a sub rectangle, but we'll get the full image here.
            var conversionParams = new XRCpuImage.ConversionParams(image, m_CameraTexture.format, transformation);

            // Texture2D allows us write directly to the raw texture data
            // This allows us to do the conversion in-place without making any copies.
            var rawTextureData = m_CameraTexture.GetRawTextureData<byte>();
            m_imageLength = rawTextureData.Length;
            unsafe
            {
                m_imagePtr = new IntPtr(rawTextureData.GetUnsafePtr());
                image.Convert(conversionParams, m_imagePtr, m_imageLength);
            }
            // Apply the updated texture data to our texture
            m_CameraTexture.Apply();
        }
    }

}
