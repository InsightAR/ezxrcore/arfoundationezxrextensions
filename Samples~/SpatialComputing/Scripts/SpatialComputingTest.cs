using System;
using EzxrCore.Common;
using EZXRCoreExtensions.SpatialComputing;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class SpatialComputingTest : MonoBehaviour
{
    public SpatialComputingController m_SpatialComputingController;
    public CpuImageController m_CpuImageController;
    public GameObject m_modelGO;
    public GameObject m_rawImageGO;
    public RawImage m_RawCameraImage;
    public string[] m_TestImage;
    public Material m_Material;
    public Camera m_ARCamera;


    private Camera m_EditorCamra;
    private Text m_ImageLocationButtonText;
    private int m_TestImageIndex;
    private string m_NowTestImage;
    private Texture2D m_CameraTexture;
    private Texture2D m_ImageTexFilpY;
    private CommandBuffer m_VideoCommandBuffer;
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_EDITOR
        GameObject childObject = new GameObject("EditorCamera");
        childObject.transform.parent = transform;
        m_EditorCamra = childObject.AddComponent<Camera>();
        m_EditorCamra.targetDisplay = 1;
        m_EditorCamra.clearFlags = CameraClearFlags.SolidColor;
        m_EditorCamra.backgroundColor = Color.black;
        int xrLayerMask = LayerMask.GetMask("XR Simulation");
        m_EditorCamra.cullingMask &= ~xrLayerMask;

        m_VideoCommandBuffer = new CommandBuffer();
        m_EditorCamra.AddCommandBuffer(CameraEvent.BeforeForwardOpaque, m_VideoCommandBuffer);

#endif  
        m_TestImageIndex = -1;
        m_ImageLocationButtonText = GameObject.Find("ImageLocationButton").GetComponentInChildren<Text>();
        if (m_SpatialComputingController != null)
        {
            m_SpatialComputingController.m_vpsRequestAuto = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // if (m_EditorCamra != null)
        // {
        //     m_VideoCommandBuffer.Clear();
        //     m_VideoCommandBuffer.Blit(m_ImageTex, BuiltinRenderTextureType.CurrentActive, m_Material); 
        // }
        #if UNITY_EDITOR
            if (m_EditorCamra != null && m_ARCamera != null)
            {
                m_EditorCamra.transform.position = m_ARCamera.transform.position;
                m_EditorCamra.transform.rotation = m_ARCamera.transform.rotation;
                m_EditorCamra.transform.localScale = m_ARCamera.transform.localScale;
            }
        #endif
    }   

    public void OnImageLocate()
    {

        m_ImageLocationButtonText.text = "下一张";
        if (m_TestImageIndex < m_TestImage.Length - 1)
            ++m_TestImageIndex;
        else
            m_TestImageIndex = 0;

        m_NowTestImage = m_TestImage[m_TestImageIndex];

        m_CameraTexture = Resources.Load<Texture2D>(m_NowTestImage);
        if (m_CameraTexture != null)
        {
#if UNITY_EDITOR
            if (m_EditorCamra != null)
            {
                Debug.Log("tex.width : " + m_CameraTexture.width + " tex.height : " + m_CameraTexture.height);
                m_EditorCamra.aspect = m_CameraTexture.width * 1.0f / m_CameraTexture.height;
                m_Material.SetTexture("_MainTex", m_CameraTexture);
                m_VideoCommandBuffer.Clear();
                m_VideoCommandBuffer.Blit(m_CameraTexture, BuiltinRenderTextureType.CurrentActive, m_Material);
            }
#endif
            m_RawCameraImage.texture = m_CameraTexture;

            m_ImageTexFilpY = Texture2DHelper.FilpY(m_CameraTexture);
            //m_ImageTexFilpY = m_CameraTexture;
            // Set the RawImage's texture so we can visualize it.
            if (m_SpatialComputingController != null)
            {
                SC_InputFrame sc_input_frame = new SC_InputFrame();
                SC_InputImage input_image = new SC_InputImage();

                //m_SpatialComputingController的timestamp和内存
                double timestamp;
                Matrix4x4 cameraToWorldMatrix;
                m_SpatialComputingController.GetTimestampAndCameraMatrix(out timestamp, out cameraToWorldMatrix);

                unsafe
                {
                    if (m_ImageTexFilpY.format != TextureFormat.RGB24)
                    {
                        Debug.LogError("image format error : " + m_ImageTexFilpY.format);
                        return;
                    }
                    var rawTextureData = m_ImageTexFilpY.GetRawTextureData<byte>();
                    IntPtr imagedataPtr = new IntPtr(rawTextureData.GetUnsafePtr());
                    int imagelength = rawTextureData.Length;
                    Debug.Log("image format : " + m_ImageTexFilpY.format + " imagelength : " + imagelength);
                    sc_input_frame.input_image = input_image;
                    input_image.format = SC_ImageFormat.SC_Image_RGB_888;
                    input_image.ptr0 = imagedataPtr;
                    input_image.len_ptr0 = imagelength;
                    input_image.height = m_ImageTexFilpY.height;
                    input_image.width = m_ImageTexFilpY.width;

                    input_image.fx = (m_ImageTexFilpY.width + m_ImageTexFilpY.height) / 2.0f; ; //
                    input_image.fy = input_image.fx;
                    input_image.cx = m_ImageTexFilpY.width / 2; // 居中
                    input_image.cy = m_ImageTexFilpY.height / 2;
                    input_image.timestamp_s = timestamp;
                    input_image.k1 = 0.0f;
                    input_image.k2 = 0.0f;
                    input_image.k3_p1 = 0.0f;
                    input_image.k4_p2 = 0.0f;
                    input_image.camera_type = SC_CameraType.SC_PINHOLE;
                }
                sc_input_frame.input_image = input_image;
                sc_input_frame.valid_T_cv_head_to_world = true; // 必须设置成true，不要走融合定位的查询逻辑
                CoordinateTransformation.ConvertUnityPoseToCvPose_c2w(cameraToWorldMatrix, out sc_input_frame.T_cv_head_to_world);

                m_SpatialComputingController.SendVPSRequestFrame(sc_input_frame);
            }
        }
    }

    public void OnLocate()
    {
        if (m_SpatialComputingController != null)
        {
            m_SpatialComputingController.SetVPSRequestManual(true);
        }
        if(m_CpuImageController != null)
        {
            m_CpuImageController.m_genCameraTexture = true;
            m_RawCameraImage.texture = m_CpuImageController.m_CameraTexture;
        }
    }

    public void onAutoLocateToggleValueChanged(bool isOn)
    {
        if (m_SpatialComputingController != null)
        {
            m_SpatialComputingController.m_vpsRequestAuto = isOn;
        }
        if(m_CpuImageController != null)
        {
            m_CpuImageController.m_genCameraTexture = isOn;
            m_RawCameraImage.texture = m_CpuImageController.m_CameraTexture;
        }
    }

    public void onImageShowToggleValueChanged(bool value)
    {
        if (true == value)
        {
            if (m_rawImageGO != null)
            {
                m_rawImageGO.SetActive(true);
            }
        }
        else
        {
            if (m_rawImageGO != null)
            {
               m_rawImageGO.SetActive(false);
            }
        }
    }

    public void OnModelShowToggleValueChanged(bool value)
    {
        if (true == value)
        {
            if (m_modelGO != null)
            {
                m_modelGO.SetActive(true);
            }
        }
        else
        {
            if (m_modelGO != null)
            {
                m_modelGO.SetActive(false);
            }
        }
    }


}
