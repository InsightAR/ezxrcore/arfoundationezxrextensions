using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using EzxrCore.Common;
using EzxrCore.Log;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace EZXRCoreExtensions.ObjectDetection
{
    public class ObjectDetectionManager : MonoBehaviour
    {
        public CopyStreamingAssets m_copyStreamingAssets;
        public ObjectDetTrackInfoController m_objectDetTrackInfoController;
        public ARCameraManager m_cameraManager;
        private ObjectDetectionController m_objectDetectionController;

        private bool m_fisrtFrameArrived = true;
        private int m_stepProcessFrame = 1;
        private int m_frameCount = 0;

        private TaskQueue m_taskQueue;
        private Task m_detectAndTrackTask;

        // Start is called before the first frame update
        void Aweak()
        {

        }

        void Start()
        {
            m_taskQueue = TaskQueue.CreateSerialQueue();
        }

        void OnEnable()
        {
            if (m_cameraManager == null)
            {
                UnityEngine.Debug.LogException(new NullReferenceException(
                    $"Serialized properties were not initialized on {name}'s {nameof(ARCameraManager)} component."), this);
                return;
            }
            if (m_copyStreamingAssets == null)
            {
                UnityEngine.Debug.LogException(new NullReferenceException(
                    $"Serialized properties were not initialized on {name}'s {nameof(CopyStreamingAssets)} component."), this);
                return;
            }
            if (m_objectDetTrackInfoController == null)
            {
                UnityEngine.Debug.LogException(new NullReferenceException(
                    $"Serialized properties were not initialized on {name}'s {nameof(ObjectDetTrackInfoController)} component."), this);
                return;
            }
            m_cameraManager.frameReceived += OnCameraFrameReceived;

            if (m_copyStreamingAssets != null)
            {
                m_copyStreamingAssets.OnCopyCompleted += OnCopyCompleted;
            }
        }

        void OnDisable()
        {
            if (m_cameraManager != null)
                m_cameraManager.frameReceived -= OnCameraFrameReceived;
        }

        void OnDestroy()
        {
            if (m_copyStreamingAssets != null)
            {
                m_copyStreamingAssets.OnCopyCompleted -= OnCopyCompleted;
            }
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                if (m_objectDetectionController != null)
                {
                    m_taskQueue?.RunAsync(() =>
                    {
                        m_objectDetectionController.StopDecetion();
                        m_objectDetectionController.StopTracking();
                        m_objectDetectionController = null;
                    });
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnCopyCompleted(string assetPath)
        {
            if (assetPath == null)
            {
                UnityEngine.Debug.LogError("configuration resources assetPath is error");
            }
            UnityEngine.Debug.Log("OnCopyCompleted : " + assetPath);

            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                if (m_objectDetectionController == null)
                {
                    m_taskQueue?.RunAsync(() =>
                    {
                        m_objectDetectionController = new ObjectDetectionController();
                        m_objectDetectionController.StartDecetion(assetPath);
                        m_objectDetectionController.StartTracking();
                    });
                }
            }
        }

        void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
        {
            if (m_fisrtFrameArrived)
            {
                XRCameraConfiguration? cameraConfiguration = m_cameraManager.currentConfiguration;
                if (cameraConfiguration.HasValue)
                {
                    XRCameraConfiguration config = cameraConfiguration.Value;
                    // Use the camera configuration here
                    UnityEngine.Debug.Log("cameraConfiguration : " + config.ToString());
                    UnityEngine.Debug.Log("cameraConfiguration config.framerate: " + config.framerate.ToString());
                    m_fisrtFrameArrived = false;
                    m_stepProcessFrame = config.framerate > 30 ? 2 : 1;
                }
            }
            m_frameCount++;
            if (m_stepProcessFrame > 1 && m_frameCount % m_stepProcessFrame == 0)
            {
                return;
            }

            // Attempt to get the latest camera image. If this method succeeds,
            // it acquires a native resource that must be disposed (see below).
            if (!m_cameraManager.TryAcquireLatestCpuImage(out XRCpuImage image))
            {
                return;
            }

            XRCameraIntrinsics intrinsics;
            if (!m_cameraManager.TryGetIntrinsics(out intrinsics))
            {
                image.Dispose();
                return;
            }

            if(m_detectAndTrackTask != null && !m_detectAndTrackTask.IsCompleted)
            {
                image.Dispose();
                return;
            }

            if ( (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) && 
                m_objectDetectionController != null)
            {
                StartCoroutine(ProcessImageAsync(intrinsics, image));
            }
            else
            {
                image.Dispose();
                return;
            }
        }

        IEnumerator ProcessImageAsync(XRCameraIntrinsics intrinsics, XRCpuImage image)
        {
            //Stopwatch stopwatch0 = new Stopwatch();
            //stopwatch0.Start();
            m_detectAndTrackTask = m_taskQueue?.RunAsync(() =>
            {
                //UnityEngine.Debug.Log("Thread ID: " + System.Threading.Thread.CurrentThread.ManagedThreadId);
                //XRCpuImage image convert to IASImage
                IASInputImage inputImage = GetIASInputImage(intrinsics, image);
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                int object_num = m_objectDetectionController.RunDetect(inputImage);
                stopwatch.Stop();
                ECLog.AddDebugLog("RunDetect time: " + stopwatch.ElapsedMilliseconds + "ms", 1);
                Stopwatch stopwatch1 = new Stopwatch();
                stopwatch1.Start();
                List<ObjectDetTrackInfo> objectDetTrackInfos = m_objectDetectionController.RunTrack(object_num);
                stopwatch1.Stop();
                ECLog.AddDebugLog("RunTrack time: " + stopwatch1.ElapsedMilliseconds + "ms", 2);
                if (objectDetTrackInfos != null)
                {
                    Loom.QueueOnMainThread(o =>
                    {
                        m_objectDetTrackInfoController?.UpdateObjectDetTrackInfo(objectDetTrackInfos, image.width, image.height);
                    }, null);
                }
            });
            // Wait for the computation to finish (if necessary)
            yield return m_detectAndTrackTask;
            image.Dispose();
            //stopwatch0.Stop();
            //ECLog.AddDebugLog("ProcessImageAsync time: " + stopwatch0.ElapsedMilliseconds + "ms", 3);
            
        }

        IASInputImage GetIASInputImage(XRCameraIntrinsics intrinsics, XRCpuImage image)
        {
            IASInputImage inputImage = new IASInputImage();
            inputImage.imgPtrType = IASImagePointerType.IASImagePointerType_RawData;
            switch (image.format)
            {
                case XRCpuImage.Format.AndroidYuv420_888:
                    inputImage.imgFormat = IASImageFormat.IASImageFormat_YUV420f;
                    break;
                case XRCpuImage.Format.IosYpCbCr420_8BiPlanarFullRange: //Y0,Y1...Yn,U0,V0,U1,V1...Un/2,Vn/2 NV21格式
                    inputImage.imgFormat = IASImageFormat.IASImageFormat_YUV420f;
                    break;
                default:
                    UnityEngine.Debug.LogError("image format is not support");
                    break;
            }
            inputImage.imgRes = new IASImageRes();
            inputImage.imgRes.width = image.width;
            inputImage.imgRes.height = image.height;

            unsafe
            {
                XRCpuImage.Plane plane = image.GetPlane(0);
                inputImage.pImg0 = new IntPtr(plane.data.GetUnsafePtr());
                XRCpuImage.Plane plane1 = image.GetPlane(1);
                inputImage.pImg1 = new IntPtr(plane1.data.GetUnsafePtr());
            }
            inputImage.timestamp = image.timestamp;
            inputImage.imgIntrinsics = new float[9] { intrinsics.focalLength.x, 0.0f, intrinsics.principalPoint.x,
                                                      0.0f, intrinsics.focalLength.y, intrinsics.principalPoint.y,
                                                      0.0f,0.0f, 1.0f};
            return inputImage;
        }
    }
}