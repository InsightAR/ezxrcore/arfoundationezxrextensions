using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXRCoreExtensions.ObjectDetection;
using EzxrCore.Common;

public class ObjectDetectionTest : MonoBehaviour
{
    public ObjectDetTrackInfoController m_objectDetTrackInfoController;
    private List<ObjectDetTrackInfo> m_ObjectDetTrackInfoList = new List<ObjectDetTrackInfo>();

    // Start is called before the first frame update
    void Start()
    {
        if (m_objectDetTrackInfoController == null)
        {
            Debug.LogException(new System.NullReferenceException(
                $"Serialized properties were not initialized on {name}'s {nameof(ObjectDetTrackInfoController)} component."), this);
            return;
        }        
        m_ObjectDetTrackInfoList.Clear();
        m_objectDetTrackInfoController.m_OnAddObjectDetTrackInfo += OnAddObjectInfo;
        m_objectDetTrackInfoController.m_OnRemoveObjectDetTrackInfo += OnRemoveObjectInfo;
        m_objectDetTrackInfoController.m_OnUpdateObjectDetTrackInfo += OnUpdateObjectInfo;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGUI()
    {
        for (int i = 0; i < m_ObjectDetTrackInfoList.Count; i++)
        {
            ObjectDetTrackInfo objectDetTrackInfo = m_ObjectDetTrackInfoList[i];
            GUIHelper.DrawRect(new Rect(objectDetTrackInfo.x1, objectDetTrackInfo.y1, objectDetTrackInfo.width, objectDetTrackInfo.height));
            string label = string.Format("Object id {0} label {1} name {2} prob {3}", objectDetTrackInfo.curr_id, objectDetTrackInfo.label, objectDetTrackInfo.class_name, objectDetTrackInfo.prob);
            //string label = string.Format("Object id {0} label {1} name {2} prob {3}", objectDetTrackInfo.curr_id, objectDetTrackInfo.label, "test", objectDetTrackInfo.prob);
            
            GUI.skin.label.fontSize = 50;
            GUI.skin.label.normal.textColor = Color.red;
            GUI.Label(new Rect(objectDetTrackInfo.x1, objectDetTrackInfo.y1, objectDetTrackInfo.width, objectDetTrackInfo.height), label);
        }
    }

    void OnDestroy()
    {
        if (m_objectDetTrackInfoController != null)
        {
            m_objectDetTrackInfoController.m_OnAddObjectDetTrackInfo -= OnAddObjectInfo;
            m_objectDetTrackInfoController.m_OnRemoveObjectDetTrackInfo -= OnRemoveObjectInfo;
            m_objectDetTrackInfoController.m_OnUpdateObjectDetTrackInfo -= OnUpdateObjectInfo;
        }
   
    }

    private void OnAddObjectInfo(List<ObjectDetTrackInfo> objectDetTrackInfos)
    {
        string strlog = "";
        foreach (var objectInfo in objectDetTrackInfos)
        {
            strlog = strlog + " " + objectInfo.curr_id;
            m_ObjectDetTrackInfoList.Add(objectInfo);
        }
        Debug.Log("OnAddObjectInfo " + strlog);
    }

    private void OnRemoveObjectInfo(List<ObjectDetTrackInfo> objectDetTrackInfos)
    {
        string strlog = "";
        foreach (var objectInfo in objectDetTrackInfos)
        {
            strlog = strlog + " " + objectInfo.curr_id;
            var go = m_ObjectDetTrackInfoList.Find(x => x.curr_id == objectInfo.curr_id);
            m_ObjectDetTrackInfoList.Remove(go);
        }
        Debug.Log("OnRemoveObjectInfo " + strlog);
    }

    private void OnUpdateObjectInfo(List<ObjectDetTrackInfo> objectDetTrackInfos)
    {
        string strlog = "";
        foreach (var objectInfo in objectDetTrackInfos)
        {
            strlog = strlog + " " + objectInfo.curr_id;
            for (int i = 0; i < m_ObjectDetTrackInfoList.Count; i++)
            {
                if (m_ObjectDetTrackInfoList[i].curr_id == objectInfo.curr_id)
                {
                    m_ObjectDetTrackInfoList[i] = objectInfo;
                    break;
                }
            }
        }
        Debug.Log("OnUpdateObjectInfo " + strlog);
    }

   
}
