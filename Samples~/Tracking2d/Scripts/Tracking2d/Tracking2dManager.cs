using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using EzxrCore.Common;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace EZXRCoreExtensions.Tracking2d
{
    public class Tracking2dManager : MonoBehaviour
    {
        public CopyStreamingAssets m_copyStreamingAssets;
        public ARCameraManager m_cameraManager;
        public AR2dMarkerInfo m_2dMarkerInfo;
        public float m_runTracking2dInterval = 2.0f;
        private Tracking2dController m_tracking2dController;
        private string m_assetPath;
        private float lastRunTracking2dTime;

        private Transform m_cameraTransform;
        void Start()
        {
            lastRunTracking2dTime = Time.time;
            m_cameraTransform = m_cameraManager.gameObject.transform;
        }

        void OnEnable()
        {
            if (m_cameraManager == null)
            {
                UnityEngine.Debug.LogException(new NullReferenceException(
                    $"Serialized properties were not initialized on {name}'s {nameof(Tracking2dManager)} component."), this);
                return;
            }

            m_cameraManager.frameReceived += OnCameraFrameReceived;

            if (m_copyStreamingAssets != null)
            {
                m_copyStreamingAssets.OnCopyCompleted += OnCopyCompleted;
            }
        }

        void OnDisable()
        {
            if (m_cameraManager != null)
                m_cameraManager.frameReceived -= OnCameraFrameReceived;
        }

        void OnDestroy()
        {
            if (m_copyStreamingAssets != null)
            {
                m_copyStreamingAssets.OnCopyCompleted -= OnCopyCompleted;
            }
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                if (m_tracking2dController != null)
                {
                    m_tracking2dController.DestroyTracking2d();
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (m_tracking2dController == null)
            {
                return;
            }

            EZXR_Detect2DResult detect2DResult = m_tracking2dController.GetDetectedMarker();
            if (detect2DResult.state == (int)EZXR_Detect2DState.EZXR_Detect2DState_Tracking)
            {
                Matrix4x4 cameraToWorldMatrix = Matrix4x4.identity;
                // Convert the  EZXR_Detect2DResult pose's transform to a Matrix4x4
                cameraToWorldMatrix.SetRow(0, new Vector4(detect2DResult.imagePose.transform[0], detect2DResult.imagePose.transform[1], detect2DResult.imagePose.transform[2], detect2DResult.imagePose.transform[3]));
                cameraToWorldMatrix.SetRow(1, new Vector4(detect2DResult.imagePose.transform[4], detect2DResult.imagePose.transform[5], detect2DResult.imagePose.transform[6], detect2DResult.imagePose.transform[7]));
                cameraToWorldMatrix.SetRow(2, new Vector4(detect2DResult.imagePose.transform[8], detect2DResult.imagePose.transform[9], detect2DResult.imagePose.transform[10], detect2DResult.imagePose.transform[11]));
                cameraToWorldMatrix.SetRow(3, new Vector4(detect2DResult.imagePose.transform[12], detect2DResult.imagePose.transform[13], detect2DResult.imagePose.transform[14], detect2DResult.imagePose.transform[15]));
                m_2dMarkerInfo?.SetMarkerInfo(cameraToWorldMatrix, detect2DResult.markerAnchor);
            }
        }

        void OnCopyCompleted(string assetPath)
        {
            if (assetPath == null)
            {
                UnityEngine.Debug.LogError("configuration resources assetPath is error");
            }
            UnityEngine.Debug.Log("OnCopyCompleted : " + assetPath);

            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                if (m_tracking2dController == null)
                {
                    m_assetPath = assetPath;
                    m_tracking2dController = new Tracking2dController();
                    int state = m_tracking2dController.CreateTracking2d(m_assetPath);
                    if (state == 0)
                    {
                        UnityEngine.Debug.Log("CreateTracking2d failed");
                        m_tracking2dController = null;
                    }
                    else
                    {
                        UnityEngine.Debug.Log("CreateTracking2d success");
                    }
                }
            }
        }

        void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
        {
            if (ARSession.state != ARSessionState.SessionTracking)
            {
                Debug.Log("ARSession.state == " + ARSession.state);
                return;
            }

            float currentTime = Time.time;
            // Calculate the time interval since the last update
            float deltaTime = currentTime - lastRunTracking2dTime;
            if (deltaTime < m_runTracking2dInterval)
            {
                return;
            }
            lastRunTracking2dTime = currentTime;
            // Attempt to get the latest camera image. If this method succeeds,
            // it acquires a native resource that must be disposed (see below).
            if (!m_cameraManager.TryAcquireLatestCpuImage(out XRCpuImage image))
            {
                return;
            }

            XRCameraIntrinsics intrinsics;
            if (!m_cameraManager.TryGetIntrinsics(out intrinsics))
            {
                image.Dispose();
                return;
            }

            if ((Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) &&
                m_tracking2dController != null)
            {
                Matrix4x4 cameraToWorldMatrix = m_cameraTransform.localToWorldMatrix;
                ProcessImage(cameraToWorldMatrix, intrinsics, image);
            }
            image.Dispose();
        }


        void ProcessImage(Matrix4x4 cameraToWorldMatrix, XRCameraIntrinsics intrinsics, XRCpuImage image)
        {
            int width = image.width;
            int height = image.height;
            float[] intrinsicsArray = new float[8] { intrinsics.focalLength.x, intrinsics.focalLength.y, intrinsics.principalPoint.x, intrinsics.principalPoint.y, 0.0f, 0.0f, 0.0f, 0.0f };
            float[] cameraToWorldMatrix_array = new float[16];
            for (int i = 0; i < 4; i++)
            {
                cameraToWorldMatrix_array[i * 4] = cameraToWorldMatrix.GetRow(i).x;
                cameraToWorldMatrix_array[i * 4 + 1] = cameraToWorldMatrix.GetRow(i).y;
                cameraToWorldMatrix_array[i * 4 + 2] = cameraToWorldMatrix.GetRow(i).z;
                cameraToWorldMatrix_array[i * 4 + 3] = cameraToWorldMatrix.GetRow(i).w;
            }
            double timestamp = image.timestamp;
            IntPtr imagePtr = IntPtr.Zero;
            unsafe
            {
                XRCpuImage.Plane plane = image.GetPlane(0);
                imagePtr = new IntPtr(plane.data.GetUnsafePtr());
            }

            int state = m_tracking2dController.SetImage(imagePtr, cameraToWorldMatrix_array, intrinsicsArray, timestamp, width, height, (int)EZVIOImageFormat.EZVIOImageFormat_GREY);
            if (state == 1)
            {
                UnityEngine.Debug.Log("SetImage success");
            }
            else
            {
                UnityEngine.Debug.Log("SetImage failed");
            }
        }

        void printfFloatArray(float[] array, string tag = "")
        {
            string str = tag + ": ";
            for (int i = 0; i < array.Length; i++)
            {
                str += array[i].ToString() + " ";
            }
            UnityEngine.Debug.Log(str);
        }
    }
}