using System.Collections.Generic;
using EzxrCore.Common;
using UnityEngine;
namespace EZXRCoreExtensions.Tracking2d
{
    public struct MarkerAnchorInfo
    {
        public Matrix4x4 pose;
        public string name;
        public Vector3 physicalSize;
        public bool isValid;

        public override string ToString()
        {
            return string.Format("name:{0}, pose:{1}, physicalSize:{2}, isValid:{3}", name, pose, physicalSize, isValid);
        }
    }

    public class MarkerAnchorInfoComparer : IEqualityComparer<MarkerAnchorInfo>
    {
        public bool Equals(MarkerAnchorInfo x, MarkerAnchorInfo y)
        {
            return x.name.Equals(y.name);
        }

        public int GetHashCode(MarkerAnchorInfo obj)
        {
            return obj.name.GetHashCode();
        }
    }

    public class AR2dMarkerInfo : MonoBehaviour
    {
        [SerializeField]
        GameObject m_AnchorPrefab;

        private List<MarkerAnchorInfo> m_markerAnchorInfos = new List<MarkerAnchorInfo>();
        private List<GameObject> m_markerAnchorObjects = new List<GameObject>();
        // Start is called before the first frame update
        void Start()
        {
            m_markerAnchorInfos.Clear();
        }

        // Update is called once per frame
        void Update()
        {
            UpdateMarkerInfo();
        }

        public void SetMarkerInfo(Matrix4x4 cameraToWorldMatrix, EZXR_MarkerAnchor markerAnchor)
        {
            m_markerAnchorInfos.Clear();
            Matrix4x4 pose_c2image = CoordinateTransformation.ConvertImagePoseToUnityPose_c2w(markerAnchor.pose.transform);
            Matrix4x4 pose_c2image_orientation = CoordinateTransformation.GetMatrixFromLandscapeLeft(Screen.orientation, pose_c2image);
            Matrix4x4 pose_image2w = cameraToWorldMatrix * pose_c2image_orientation.inverse;
            var markerAnchorInfo = new MarkerAnchorInfo()
            {
                pose = pose_image2w,
                name = markerAnchor.name,
                physicalSize =  new Vector3(markerAnchor.physicalSize[0], 0.01f, markerAnchor.physicalSize[1]),
                isValid = markerAnchor.isValid
            };
        
            bool isExist = false;
            for (int j = 0; j < m_markerAnchorInfos.Count; j++)
            {
                if (m_markerAnchorInfos[j].name.Equals(markerAnchorInfo.name))
                {
                    m_markerAnchorInfos[j] = markerAnchorInfo;
                    isExist = true;
                    break;
                }
            }
            if (!isExist)
            {
                m_markerAnchorInfos.Add(markerAnchorInfo);
            }
        }

        private void UpdateMarkerInfo()
        {
            for (int i = 0; i < m_markerAnchorInfos.Count; i++)
            {
                Debug.Log("markerAnchorInfo " + i + "/" + m_markerAnchorInfos.Count + " : " + m_markerAnchorInfos[i].ToString());
                GameObject anchor = null;
                if (m_markerAnchorObjects.Count > i)
                {
                    anchor = m_markerAnchorObjects[i];
                    m_markerAnchorObjects[i].transform.position = m_markerAnchorInfos[i].pose.GetPosition();
                    m_markerAnchorObjects[i].transform.rotation = m_markerAnchorInfos[i].pose.rotation;
                    m_markerAnchorObjects[i].transform.localScale = m_markerAnchorInfos[i].physicalSize;
                }
                else
                {
                    anchor = Instantiate(m_AnchorPrefab, m_markerAnchorInfos[i].pose.GetPosition(), m_markerAnchorInfos[i].pose.rotation);
                    anchor.transform.localScale = m_markerAnchorInfos[i].physicalSize;
                    anchor.name = m_markerAnchorInfos[i].name;
                    m_markerAnchorObjects.Add(anchor);
                }
                anchor.SetActive(m_markerAnchorInfos[i].isValid);
            }
        }
    }
}